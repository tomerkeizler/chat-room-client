import socketIOClient from "socket.io-client";
import { useEffect, useRef, useState } from "react";
import {
  SERVER_URL,
  CHAT_ROOM_SYSTEM_MESSAGE,
  CHAT_MESSAGE,
  USER_TYPING,
  USER_STOPPED_TYPING,
} from "../CONSTANTS";

// ----------------------------

const useChat = (ROOM_ID) => {
  const socketRef = useRef();
  const [messages, setMessages] = useState([]);
  const [otherTypingUserID, setOtherTypingUserID] = useState(null);
  const [isTyping, setIsTyping] = useState(false);
  const timeoutRef = useRef();

  useEffect(() => {
    socketRef.current = socketIOClient(SERVER_URL, {
      query: { ROOM_ID },
    });

    socketRef.current.on(CHAT_ROOM_SYSTEM_MESSAGE, (message) => {
      const incomingMessage = {
        ...message,
        isSystemMessage: true,
      };
      setMessages((messages) => [...messages, incomingMessage]);
    });

    socketRef.current.on(CHAT_MESSAGE, (message) => {
      const incomingMessage = {
        ...message,
        isSelfMessage: message.senderID === socketRef.current.id,
      };
      setMessages((messages) => [...messages, incomingMessage]);
    });

    socketRef.current.on(USER_TYPING, (message) => {
      setOtherTypingUserID(message.senderID);
    });

    socketRef.current.on(USER_STOPPED_TYPING, (message) => {
      setOtherTypingUserID(null);
    });

    return () => {
      socketRef.current.disconnect();
    };
  }, [ROOM_ID]);

  // ----------------------------

  const sendMessage = (body) => {
    socketRef.current.emit(CHAT_MESSAGE, {
      body,
      senderID: socketRef.current.id,
    });
  };

  // ----------------------------

  const setTypingTimeOut = () => {
    timeoutRef.current = setTimeout(handleStoppedTyping, 2500);
  };

  const handleStartedTyping = () => {
    setIsTyping(true);
    socketRef.current.emit(USER_TYPING, {
      senderID: socketRef.current.id,
    });
  };

  const handleStoppedTyping = () => {
    setIsTyping(false);
    socketRef.current.emit(USER_STOPPED_TYPING, {
      senderID: socketRef.current.id,
    });
  };

  const handleSelfTyping = () => {
    if (!isTyping) {
      handleStartedTyping();
      setTypingTimeOut();
    } else {
      clearTimeout(timeoutRef.current);
      setTypingTimeOut();
    }
  };

  // ----------------------------

  return { messages, handleSelfTyping, sendMessage, otherTypingUserID };
};

export default useChat;
