import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import ChatLogin from "./ChatLogin";
import ChatRoom from "./ChatRoom";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={ChatLogin} />
        <Route exact path="/:ROOM_ID" component={ChatRoom} />
      </Switch>
    </BrowserRouter>
  );
};

export default App;
