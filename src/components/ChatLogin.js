import React, { useState } from "react";
import styled from "styled-components";
import { Link } from "react-router-dom";

const ChatLogin = () => {
  const [roomName, setRoomName] = useState("");

  return (
    <Container>
      <Title>Welcome to Rapyd Chat!</Title>
      <Hbox>
        <Input
          type="text"
          placeholder="Room 1"
          value={roomName}
          onChange={(e) => setRoomName(e.target.value)}
        />
        <Button to={`/${roomName}`}>Join room</Button>
      </Hbox>
    </Container>
  );
};

export default ChatLogin;

// ----------------------------

const Container = styled.div`
  position: fixed;
  left: 25%;
  right: 25%;
  top: 30%;

  // padding: 5px;
  border: 3px solid black;

  display: flex;
  flex-direction: column;
`;

// ----------------------------

const Title = styled.span`
  margin: 5px;
  padding: 10px;

  background: gainsboro;
  font-size: 40px;
  font-weight: 600;
  text-align: center;
`;

// ----------------------------

const Hbox = styled.div`
  display: flex;
  flex-direction: row;
`;

// ----------------------------

const Input = styled.input`
  margin: 5px;
  padding: 10px;
  outline: none;
  font-size: 25px;
  width: 65%;
`;

// ----------------------------

const Button = styled(Link)`
  width: 35%;
  margin: 5px;
  padding: 8px;

  background: MediumSeaGreen;
  font-size: 30px;
  font-weight: 600;
  color: white;
  text-decoration: none;
  text-align: center;
`;
