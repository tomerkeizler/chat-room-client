import React, { useState, useRef } from "react";
import styled from "styled-components";
import useChat from "./useChat";

const ChatRoom = (props) => {
  const { ROOM_ID } = props.match.params;
  const [message, setMessage] = useState("");
  const {
    messages,
    handleSelfTyping,
    sendMessage,
    otherTypingUserID,
  } = useChat(ROOM_ID);

  const handleEditMessage = (e) => {
    setMessage(e.target.value);
    handleSelfTyping();
  };

  const handleSendMessage = () => {
    setMessage("");
    sendMessage(message);
  };

  return (
    <Container>
      <RoomTitle>Chat room: {ROOM_ID}</RoomTitle>

      <MessageBoard>
        {messages.map((msg, i) => (
          <Message
            key={i}
            isSystemMessage={msg.isSystemMessage}
            isSelfMessage={msg.isSelfMessage}
          >
            {msg.body}
          </Message>
        ))}
        {otherTypingUserID && (
          <UserTypingAlert>{otherTypingUserID} is typing...</UserTypingAlert>
        )}
      </MessageBoard>

      <Textarea
        value={message}
        placeholder="Write a message..."
        onChange={handleEditMessage}
      />

      <Button onClick={handleSendMessage}>Send message</Button>
    </Container>
  );
};

export default ChatRoom;

// ----------------------------

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  bottom: 0;
  right: 0;
  margin: 10px;

  display: flex;
  flex-direction: column;
`;

// ----------------------------

const RoomTitle = styled.span`
  margin: 5px 0;
  padding: 10px;

  background: gainsboro;
  font-size: 40px;
  font-weight: 600;
  text-align: center;
`;

// ----------------------------

const MessageBoard = styled.div`
  list-style-type: none;
  overflow: auto;

  margin: 5px 0;
  padding: 0;
  min-height: 60%;
  border: 2px solid black;
`;

// ----------------------------

const Message = styled.li`
  width: 50%;
  margin: 10px;
  padding: 10px;
  border-radius: 20px;

  ${({ isSystemMessage, isSelfMessage }) =>
    isSystemMessage
      ? `
      margin-left: auto;
      background-color: black;
      color: white;
      `
      : isSelfMessage
      ? `
      margin-right: auto;
      background-color: MediumSeaGreen;
      color: white;
      `
      : `
      margin-left: auto;
      background-color: gainsboro;
      color: black;
     `}
`;

// ----------------------------

const UserTypingAlert = styled(Message)`
  background-color: RoyalBlue;
  color: white;
  font-weight: 600;
`;

// ----------------------------

const Textarea = styled.textarea`
  margin: 5px 0;
  padding: 10px;
  height: 20%;
  border: 2px solid black;
  outline: none;
  font-size: 20px;
  resize: none;
`;

// ----------------------------

const Button = styled.button`
  margin: 5px 0;
  padding: 15px 10px;
  border: none;
  outline: none;

  background: MediumSeaGreen;
  font-size: 30px;
  font-weight: 600;
  color: white;
`;
