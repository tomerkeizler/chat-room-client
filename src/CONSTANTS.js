export const SERVER_URL = "http://localhost:3030"

export const CHAT_ROOM_SYSTEM_MESSAGE = "chatRoomSystemMessage";
export const CHAT_MESSAGE = "chatMessage";
export const USER_TYPING = "userTyping";
export const USER_STOPPED_TYPING = "userStoppedTyping";
